//
//  AppDelegate.h
//  ChelseaFC
//
//  Created by MAC on 30/06/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)toggleLoader:(BOOL)show withView:(UIView *)view;


@end

