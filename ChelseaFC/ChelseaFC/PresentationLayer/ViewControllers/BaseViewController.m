//
//  BaseViewController.m
//  ChelseaFC
//
//  Created by MAC on 02/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) btnLeftTopBarClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    ChelseaFCLog(@"sender.tag: %ld", (long)[sender tag]);
}

-(void) btnRightTopBarClicked:(id)sender
{
    ChelseaFCLog(@"sender.tag: %ld", (long)[sender tag]);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
