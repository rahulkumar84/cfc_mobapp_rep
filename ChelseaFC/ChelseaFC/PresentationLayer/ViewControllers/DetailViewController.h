//
//  DetailViewController.h
//  ChelseaFC
//
//  Created by MAC on 03/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DetailViewController : BaseViewController<TopBarViewDelegate>



@property(nonatomic,retain) NSString *strTitle;
@property (weak, nonatomic) IBOutlet TopBarView *viewTopBar;
@property (weak, nonatomic) IBOutlet UIView *viewMain;

@end
