//
//  HomeViewController.h
//  ChelseaFC
//
//  Created by MAC on 30/06/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : BaseViewController<TopBarViewDelegate>

@property (weak, nonatomic) IBOutlet TopBarView *viewTopBar;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlView;


- (IBAction)btnClicked:(id)sender;


@end

