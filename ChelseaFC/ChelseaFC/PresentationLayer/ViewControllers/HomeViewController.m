//
//  HomeViewController.m
//  ChelseaFC
//
//  Created by MAC on 30/06/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import "HomeViewController.h"
#import "DetailViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.viewTopBar.backgroundColor = colorClear;
//    self.viewTopBar.callBack = self;
//    [self.viewTopBar setTopBarWithTitle:@"Title" withBgImage:@"" withLeftButton:@"Back" withRightButton:@"Home"];
    
    
//    self.scrlView.contentSize = CGSizeMake(self.scrlView.frame.size.width, 1000);
    
}



-(void) btnLeftTopBarClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    ChelseaFCLog(@"sender.tag: %ld", (long)[sender tag]);
}

-(void) btnRightTopBarClicked:(id)sender
{
    ChelseaFCLog(@"sender.tag: %ld", (long)[sender tag]);
}



- (IBAction)btnClicked:(id)sender
{
    [self performSegueWithIdentifier:@"DetailViewControllerPush" sender:sender];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"DetailViewControllerPush"])
    {
        DetailViewController *objdestinationVC = (DetailViewController *)[segue destinationViewController];

        if ([sender tag] == 501)
        {
            objdestinationVC.strTitle = @"News Feed";
        }
        
        if ([sender tag] == 502)
        {
            objdestinationVC.strTitle = @"Social Stream";
        }
        
        if ([sender tag] == 503)
        {
            objdestinationVC.strTitle = @"Match Zone";
        }
    }
}

@end
