//
//  TopBarView.h
//  ChelseaFC
//
//  Created by MAC on 02/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TopBarViewDelegate <NSObject>

- (void) btnLeftTopBarClicked:(id)sender;
- (void) btnRightTopBarClicked:(id)sender;


@end

@interface TopBarView : UIView
{
    IBOutlet UIView *viewTopBar;
    IBOutlet UIImageView *imgViewTopBar;
    IBOutlet UIView *viewLeftViewTopBar;
    IBOutlet UIView *viewRightTopBar;
    IBOutlet UIView *viewTitleTopBar;
    
    IBOutlet UIButton *btnLeftTopBar;
    IBOutlet UIButton *btnRightTopBar;
    IBOutlet UILabel *lblTitleTopBar;
    IBOutlet UIImageView *imgViewHercLogo;
    
    IBOutlet NSLayoutConstraint *constrsintsRightView;
    IBOutlet NSLayoutConstraint *constraintsTitleView;
    IBOutlet NSLayoutConstraint *constraintsLeftView;
    
    IBOutlet NSLayoutConstraint *constraintsLogoLeft;
    IBOutlet NSLayoutConstraint *constraintsLogoRight;
    
    id <TopBarViewDelegate> callBack;

}

@property(nonatomic,retain)id <TopBarViewDelegate> callBack;

@property(nonatomic,retain) IBOutlet UIView *viewTopBar;
@property(nonatomic,retain) IBOutlet UIImageView *imgViewTopBar;
@property(nonatomic,retain) IBOutlet UIView *viewLeftViewTopBar;
@property(nonatomic,retain) IBOutlet UIView *viewRightTopBar;
@property(nonatomic,retain) IBOutlet UIView *viewTitleTopBar;
@property(nonatomic,retain) IBOutlet UIButton *btnLeftTopBar;
@property(nonatomic,retain) IBOutlet UIButton *btnRightTopBar;
@property(nonatomic,retain) IBOutlet UILabel *lblTitleTopBar;
@property(nonatomic,retain) IBOutlet UIImageView *imgViewHercLogo;
@property(nonatomic,retain) IBOutlet NSLayoutConstraint *constrsintsRightView;
@property(nonatomic,retain) IBOutlet NSLayoutConstraint *constraintsTitleView;
@property(nonatomic,retain) IBOutlet NSLayoutConstraint *constraintsLeftView;
@property(nonatomic,retain) IBOutlet NSLayoutConstraint *constraintsLogoLeft;
@property(nonatomic,retain) IBOutlet NSLayoutConstraint *constraintsLogoRight;


-(IBAction) btnRightTopBarClicked:(id)sender;
-(IBAction) btnLeftTopBarClicked:(id)sender;

-(void) setTopBarWithTitle:(NSString *)strTitle withBgImage:(NSString *)strBGImageName withLeftButton:(NSString *)strLeftBtnImageName withRightButton:(NSString *)strRightBtnImage;


@end
