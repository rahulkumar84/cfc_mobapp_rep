//
//  TopBarView.m
//  ChelseaFC
//
//  Created by MAC on 02/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import "TopBarView.h"

@implementation TopBarView

@synthesize callBack;

@synthesize viewTopBar;
@synthesize imgViewTopBar;
@synthesize viewLeftViewTopBar;
@synthesize viewRightTopBar;
@synthesize viewTitleTopBar;
@synthesize btnLeftTopBar;
@synthesize btnRightTopBar;
@synthesize lblTitleTopBar;
@synthesize imgViewHercLogo;
@synthesize constrsintsRightView;
@synthesize constraintsTitleView;
@synthesize constraintsLeftView;
@synthesize constraintsLogoLeft;
@synthesize constraintsLogoRight;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 64);
    }
    return self;
}



- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (id)awakeAfterUsingCoder:(NSCoder*)decoder
{
    if ([[self subviews] count] == 0)
    {
        return [self loadNib];
    }
    return self;
}

-(TopBarView *)loadNib
{
    NSArray * arr = [[NSBundle mainBundle] loadNibNamed:@"TopBarView" owner:nil options:nil];
    return [arr firstObject];
}


-(NSLayoutConstraint *) changeMultiplier:(NSLayoutConstraint *)constraint withMultiplier:(CGFloat)multiplier
{
    NSLayoutConstraint *newConstraint = [NSLayoutConstraint  constraintWithItem:[constraint firstItem] attribute:[constraint firstAttribute] relatedBy:[constraint relation] toItem:[constraint secondItem] attribute:[constraint secondAttribute] multiplier:multiplier constant:[constraint constant]];
    newConstraint.priority = [constraint priority];
//    [NSLayoutConstraint deactivateConstraints:constraint];
//    [NSLayoutConstraint activateConstraints:constraint];
    return newConstraint;
}


-(void) setTopBarWithTitle:(NSString *)strTitle withBgImage:(NSString *)strBGImageName withLeftButton:(NSString *)strLeftBtnImageName withRightButton:(NSString *)strRightBtnImage
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 64);
        
    CGFloat smallRatio = 0.16;
    CGFloat bigRatio = 0.24;
    CGFloat remainRatio = 1.0 - (smallRatio + smallRatio);
    
    viewTopBar.backgroundColor = colorBlue;
    
    // for default and LSmall RSmall
    [self changeMultiplier:constrsintsRightView withMultiplier:smallRatio];
    [self changeMultiplier:constraintsLeftView withMultiplier:smallRatio];
    [self changeMultiplier:constraintsTitleView withMultiplier:remainRatio];
    
    // BG Image
    if ([[strBGImageName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] !=0)
    {
        if([strBGImageName isEqualToString:kNavBar_LBig_RBig])
        {
            remainRatio = 1.0 - (bigRatio + bigRatio);
            [self changeMultiplier:constrsintsRightView withMultiplier:bigRatio];
            [self changeMultiplier:constraintsLeftView withMultiplier:bigRatio];
            [self changeMultiplier:constraintsTitleView withMultiplier:remainRatio];
        }
        else if([strBGImageName isEqualToString:kNavBar_LSmall_RBig])
        {
            remainRatio = 1.0 - (bigRatio + smallRatio);
            [self changeMultiplier:constrsintsRightView withMultiplier:bigRatio];
            [self changeMultiplier:constraintsLeftView withMultiplier:smallRatio];
            [self changeMultiplier:constraintsTitleView withMultiplier:remainRatio];
        }
        else if([strBGImageName isEqualToString:kNavBar_LBig] || [strBGImageName isEqualToString:kNavBar_LBig_RSmall])
        {
            remainRatio = 1.0 - (bigRatio + smallRatio);
            [self changeMultiplier:constrsintsRightView withMultiplier:smallRatio];
            [self changeMultiplier:constraintsLeftView withMultiplier:bigRatio];
            [self changeMultiplier:constraintsTitleView withMultiplier:remainRatio];
        }

        
        imgViewTopBar.image = [UIImage imageNamed:strBGImageName];
    }
    else
    {
        imgViewTopBar.backgroundColor = colorBlue;
    }
    
    // Title
    if (strTitle.length != 0)
    {
        lblTitleTopBar.hidden = NO;
        imgViewHercLogo.hidden = YES;
        lblTitleTopBar.text = strTitle;
    }
    else
    {
        lblTitleTopBar.hidden = YES;
        imgViewHercLogo.hidden = NO;
    }
    
    
    // Left Button Images
    if ([[strLeftBtnImageName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] !=0)
    {
        NSString *strExt = [strLeftBtnImageName pathExtension];
        ChelseaFCLog(@"strExt: %@", strExt);
        btnLeftTopBar.hidden = NO;
        
        if ([[strExt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] ==0)
        {
            [btnLeftTopBar setTitle:strLeftBtnImageName forState:UIControlStateNormal];
            [btnLeftTopBar setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
        else
        {
            [btnLeftTopBar setImage:[UIImage imageNamed:strLeftBtnImageName] forState:UIControlStateNormal];
        }
    }
    else
    {
        btnLeftTopBar.hidden = YES;
    }
    
    
    // Right Button Images
    if ([[strRightBtnImage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] !=0)
    {
        NSString *strExt = [strLeftBtnImageName pathExtension];
        ChelseaFCLog(@"strExt: %@", strExt);
        btnRightTopBar.hidden = NO;
        
        if ([[strExt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] ==0)
        {
            [btnRightTopBar setTitle:strRightBtnImage forState:UIControlStateNormal];
            [btnRightTopBar setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
        else
        {
            [btnRightTopBar setImage:[UIImage imageNamed:strRightBtnImage] forState:UIControlStateNormal];
        }
    }
    else
    {
        btnRightTopBar.hidden = YES;
    }
    
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(self.superview.mas_top).offset(0);
         make.left.equalTo(self.superview.mas_left).offset(0);
         make.height.equalTo(@64);
         make.right.equalTo(self.superview.mas_right).offset(0);
    }];
    
}


-(IBAction) btnRightTopBarClicked:(id)sender
{
    if ([self callBack] != nil)
    {
        if ([self.callBack respondsToSelector:@selector(btnRightTopBarClicked:)]) {
            [self.callBack performSelector:@selector(btnRightTopBarClicked:) withObject:sender];
        }
    }
}

-(IBAction) btnLeftTopBarClicked:(id)sender
{
    if ([self callBack] != nil)
    {
        if ([self.callBack respondsToSelector:@selector(btnLeftTopBarClicked:)]) {
            [self.callBack performSelector:@selector(btnLeftTopBarClicked:) withObject:sender];
        }
    }
}


@end
