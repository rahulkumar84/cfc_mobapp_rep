//
//  ColorConstatnt.h
//  ChelseaFC
//
//  Created by MAC on 12/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <Foundation/Foundation.h>

// color
#define colorVeryLightGrey [UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0]
#define colorDarkRed [UIColor colorWithRed:175.0/255.0 green:35.0/255.0 blue:4.0/255.0 alpha:1.0]

#define colorBlue [UIColor blueColor]
#define colorClear [UIColor clearColor]