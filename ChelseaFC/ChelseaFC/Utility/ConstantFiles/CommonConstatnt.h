//
//  CommonConstatnt.h
//  ChelseaFC
//
//  Created by MAC on 12/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <Foundation/Foundation.h>

// Appdelegate
#define APP_DELEGATE      ((AppDelegate *)[[UIApplication sharedApplication] delegate])


// Logs
#define ChelseaFCShowLogs  1
#define ChelseaFCLog( s, ... ) if(ChelseaFCShowLogs) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )



//
#define kNavBar_Default @"NavBar_Default.png"
#define kNavBar_LBig_RBig @"NavBar_LBig_RBig.png"
#define kNavBar_LBig @"NavBar_LBig.png"
#define kNavBar_LSmall_RBig @"NavBar_LSmall_RBig.png"
#define kNavBar_LBig_RSmall @"NavBar_LBig_RSmall.png"
#define kNavBar_LSmall @"NavBar_LSmall.png"



// Font
#define FontBoldSize(xx)  [UIFont fontWithName:@"HelveticaNeue-Bold" size:xx]
#define FontRegularSize(xx)  [UIFont fontWithName:@"Helvetica Neue" size:xx]
#define FontRegularSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter" size:xx]
#define FontCondensedSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter-Condensed" size:xx]
#define FontBoldSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter-Bold" size:xx]

// color
#define colorVeryLightGrey [UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0]
#define colorDarkRed [UIColor colorWithRed:175.0/255.0 green:35.0/255.0 blue:4.0/255.0 alpha:1.0]