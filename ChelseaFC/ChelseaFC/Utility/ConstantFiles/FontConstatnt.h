//
//  FontConstatnt.h
//  ChelseaFC
//
//  Created by MAC on 12/07/16.
//  Copyright © 2016 Wipro. All rights reserved.
//

#import <Foundation/Foundation.h>


// Font
#define FontBoldSize(xx)  [UIFont fontWithName:@"HelveticaNeue-Bold" size:xx]
#define FontRegularSize(xx)  [UIFont fontWithName:@"Helvetica Neue" size:xx]
#define FontRegularSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter" size:xx]
#define FontCondensedSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter-Condensed" size:xx]
#define FontBoldSizeAmericanTW(xx)  [UIFont fontWithName:@"AmericanTypewriter-Bold" size:xx]
